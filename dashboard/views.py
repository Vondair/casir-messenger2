# -*- coding: utf-8 -*-

# Standard library imports
from __future__ import unicode_literals

from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib import messages

from members.models import Member
from members.forms import AddFriendForm, WaitingFriendsRequestForm

from messaging.models import Message

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render

@login_required
def index(request):
    current_member = request.user.member
    friends = current_member.friends.all()
    friend_requests = current_member.friend_requests.all()
    received_messages = Message.objects.filter(recipient=current_member)

    if request.method == "POST":
        add_friend_form = AddFriendForm(member=current_member, data=request.POST)
        if add_friend_form.is_valid():

            for member in add_friend_form.cleaned_data["members"]:
                member.friend_requests.add(current_member)
            add_friend_form = AddFriendForm(member=current_member)

            messages.success(request, "Demandes d'ajout effectuées avec succès")
            
        if "is_denied_request" in request.POST:
            friend_request_form = WaitingFriendsRequestForm(member=current_member, data=request.POST)
            if friend_request_form.is_valid():
                for member in friend_request_form.cleaned_data["members"]:
                    current_member.friend_requests.remove(member)
                friend_request_form = WaitingFriendsRequestForm(member=current_member)
                add_friend_form = AddFriendForm(member=current_member)
                messages.success(request, "Invitation refusée")

        if "is_accepted_request" in request.POST:
            friend_request_form = WaitingFriendsRequestForm(member=current_member, data=request.POST)
            if friend_request_form.is_valid():
                for member in friend_request_form.cleaned_data["members"]:
                    current_member.friend_requests.remove(member)
                    current_member.friends.add(member)
                friend_request_form = WaitingFriendsRequestForm (member=current_member)
                add_friend_form = AddFriendForm(member=current_member)
                messages.success(request, "Invitation acceptée")            

    elif request.method == "GET":
        add_friend_form = AddFriendForm(member=current_member)
        friend_request_form = WaitingFriendsRequestForm(member=current_member)
        
    paginator = Paginator(received_messages, 10) # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        messages_paginator = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        messages_paginator = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        messages_paginator = paginator.page(paginator.num_pages)

    context = {'friends': friends,
               'friend_requests': friend_requests,
               'add_friend_form': add_friend_form,
               'received_messages': received_messages,
               'friend_request_form': friend_request_form,
               'messages_paginator': messages_paginator}
    
    
    
    
    return render(request, "dashboard/index.html", context)


@login_required
def remove_friend(request, friend_pk):
    current_member = request.user.member
    removed_friend = get_object_or_404(Member, pk=friend_pk)
    current_member.friends.remove(removed_friend)
    messages.success(request, "Suppression effectuée avec succès")
    return redirect("dashboard:index")

@login_required
def message(request, message_pk):
    message = get_object_or_404(Message, pk=message_pk)
    if message.is_read:
        return False

    message.is_read = True
    message.save()
    url_picture = ""
    if message.picture != "":
        url_picture = message.picture.url
    json_message = {
        'picture': url_picture,
        'content': message.content,
        'duration': message.duration,
        'author': message.author.user.username
    }
    return JsonResponse(json_message)
