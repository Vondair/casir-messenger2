# -*- coding: utf-8 -*-

# Standard library imports
from __future__ import unicode_literals

from django import forms

from messaging.models import Message

class NewMessageForm():
    class Meta:
        model = Message
        fields = ('recipient', 'content', 'picture', 'duration')

    def __init__(self, member, *args, **kwargs):
        super(NewMessageForm, self).__init__(*args, **kwargs)
        friends_list = member.friends.all()
        self.fields["recipient"].queryset = friends_list
        self.fields["recipient"].widget.attrs['class'] = "col-lg-10 col-sd-10 col-md-10 col-xs-10 "